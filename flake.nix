{
    description = "lollypop";

    inputs = {
        nixpkgs.url = github:NixOS/nixpkgs;
        flake-utils.url = "github:numtide/flake-utils";
    };

    outputs = { self, flake-utils, nixpkgs }:
        flake-utils.lib.eachDefaultSystem (system:
            let
                pkgs = import nixpkgs { inherit system; };

                lollypop = pkgs.python3.pkgs.buildPythonApplication rec {
                    pname = "lollypop";
                    version = "1.4.23";

                    format = "other";
                    doCheck = false;

                    src = pkgs.fetchgit {
                        url = "https://gitlab.gnome.org/World/lollypop";
                        rev = "refs/tags/${version}";
                        fetchSubmodules = true;
                        sha256 = "wwdH3gMpYt40VGqrL1XfB1dOfg45zLKtTEI23AwjCis=";
                    };

                    nativeBuildInputs = with pkgs; [
                        appstream-glib
                        desktop-file-utils
                        gobject-introspection
                        meson
                        ninja
                        pkg-config
                        wrapGAppsHook
                    ];

                    buildInputs = with pkgs; with gst_all_1; [
                        gdk-pixbuf
                        glib
                        glib-networking
                        gst-libav
                        gst-plugins-bad
                        gst-plugins-base
                        gst-plugins-good
                        gst-plugins-ugly
                        gstreamer
                        gtk3
                        libhandy
                        libsoup
                        pango
                        totem-pl-parser

                        # lastfm support
                        libsecret
                    ];

                    propagatedBuildInputs = with pkgs.python3.pkgs; [
                        beautifulsoup4
                        pillow
                        pycairo
                        pygobject3

                        # lastfm support
                        pylast

                        # youtube support
                        youtube-dl
                    ];

                    patches = [ ./fix-listenbrainz.patch ];

                    postPatch = ''
                        chmod +x meson_post_install.py
                        patchShebangs meson_post_install.py
                    '';

                    postFixup = ''
                        wrapPythonProgramsIn $out/libexec "$out $propagatedBuildInputs"
                    '';

                    strictDeps = false;

                    # Produce only one wrapper using wrap-python passing
                    # gappsWrapperArgs to wrap-python additional wrapper
                    # argument
                    dontWrapGApps = true;

                    preFixup = ''
                        makeWrapperArgs+=("''${gappsWrapperArgs[@]}")
                    '';

                    passthru = {
                        updateScript = pkgs.nix-update-script {
                            attrPath = pname;
                        };
                    };

                    meta = with pkgs.lib; {
                        changelog = "https://gitlab.gnome.org/World/lollypop/tags/${version}";
                        description = "A modern music player for GNOME";
                        homepage = "https://wiki.gnome.org/Apps/Lollypop";
                        license = licenses.gpl3Plus;
                        maintainers = with maintainers; [ lovesegfault ];
                        platforms = platforms.linux;
                    };
                };
            in {
                packages.lollypop = lollypop;
                defaultPackage = lollypop;
            }
        );
}
